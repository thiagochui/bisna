<?php

	$portfolio_metabox = new Odin_Metabox(
	    'portfolio_metabox', // Slug/ID do Metabox (obrigatório)
	    'Trabalhos', // Nome do Metabox  (obrigatório)
	    'portfolio' // Slug do Post Type, sendo possível enviar apenas um valor ou um array com vários (opcional)
	);

	$portfolio_metabox->set_fields(
	    array(
	        array(
			    'id'          => 'img_destaque', // Obrigatório
			    'label'       => __( 'Imagem Destaque', 'odin' ), // Obrigatório
			    'type'        => 'image', // Obrigatório
			    'default'     => '', // Opcional (deve ser o id de uma imagem em mídia)
			    'description' => __( 'Imagem Destaque para Home', 'odin' ), // Opcional
			),
			array(
			    'id'          => 'img_topo', // Obrigatório
			    'label'       => __( 'Imagem Topo', 'odin' ), // Obrigatório
			    'type'        => 'image', // Obrigatório
			    'default'     => '', // Opcional (deve ser o id de uma imagem em mídia)
			    'description' => __( 'Imagem para o Topo do Projeto', 'odin' ), // Opcional
			),
			array(
			    'id'          => 'galeria_imagens', // Obrigatório
			    'label'       => __( 'Galeria de Imagens', 'odin' ), // Obrigatório
			    'type'        => 'image_plupload', // Obrigatório
			    'default'     => '', // Opcional (deve ser o id de uma imagem em mídias, separe os ids com virtula)
			    'description' => __( 'Adicione imagens para a galeria do projeto', 'odin' ), // Opcional
			)
	    )
	);

	// MetaBox Home
	$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
	$template_file = get_post_meta($post_id,'_wp_page_template',TRUE);
	if ($template_file == 'page-home.php')
	{
		$home_metabox = new Odin_Metabox(
		    'home', // Slug/ID do Metabox (obrigatório)
		    'Configurações da Home', // Nome do Metabox  (obrigatório)
		    'page' // Slug do Post Type, sendo possível enviar apenas um valor ou um array com vários (opcional)
		);

		$home_metabox->set_fields(
		    array(
		        array(
				    'id'          => 'sobre_nos', // Obrigatório
				    'label'       => __( 'Sobre Nós', 'odin' ), // Obrigatório
				    'type'        => 'editor', // Obrigatório
				    'options'     => array( // Opcional (aceita argumentos do wp_editor)
				        'textarea_rows' => 10
				    ),
				),
				array(
				    'id'   => 'separator_1', // Obrigatório
				    'type' => 'separator' // Obrigatório
				),
				array(
				    'id'   => 'padawans', // Obrigatório
				    'label'=> __( 'Os Padawans', 'odin' ), // Obrigatório
				    'type' => 'title', // Obrigatório
				),
				array(
				    'id'          => 'img_padawan_1', // Obrigatório
				    'label'       => __( 'Imagem do Padawan', 'odin' ), // Obrigatório
				    'type'        => 'image', // Obrigatório
				    'default'     => '', // Opcional (deve ser o id de uma imagem em mídia)
				),
				array(
				    'id'          => 'nome_padawan_1', // Obrigatório
				    'label'       => __( 'Nome do Padawan', 'odin' ), // Obrigatório
				    'type'        => 'text', // Obrigatório
				),
				array(
				    'id'          => 'texto_padawan_1', // Obrigatório
				    'label'       => __( 'Sobre o Padawan', 'odin' ), // Obrigatório
				    'type'        => 'editor', // Obrigatório
				    'options'     => array( // Opcional (aceita argumentos do wp_editor)
				        'textarea_rows' => 10
				    ),  
				),
				array(
				    'id'          => 'img_padawan_2', // Obrigatório
				    'label'       => __( 'Imagem do Padawan', 'odin' ), // Obrigatório
				    'type'        => 'image', // Obrigatório
				    'default'     => '', // Opcional (deve ser o id de uma imagem em mídia)
				),
				array(
				    'id'          => 'nome_padawan_2', // Obrigatório
				    'label'       => __( 'Nome do Padawan', 'odin' ), // Obrigatório
				    'type'        => 'text', // Obrigatório
				),
				array(
				    'id'          => 'texto_padawan_2', // Obrigatório
				    'label'       => __( 'Sobre o Padawan', 'odin' ), // Obrigatório
				    'type'        => 'editor', // Obrigatório
				    'options'     => array( // Opcional (aceita argumentos do wp_editor)
				        'textarea_rows' => 10
				    ),  
				),
				array(
				    'id'   => 'separator_2', // Obrigatório
				    'type' => 'separator' // Obrigatório
				),
				array(
				    'id'   => 'contato', // Obrigatório
				    'label'=> __( 'Contato', 'odin' ), // Obrigatório
				    'type' => 'title', // Obrigatório
				),
				array(
				    'id'          => 'texto_trabalhe', // Obrigatório
				    'label'       => __( 'Trabalhe com a gente', 'odin' ), // Obrigatório
				    'type'        => 'text', // Obrigatório
				),
				array(
				    'id'          => 'texto_contrata', // Obrigatório
				    'label'       => __( 'Contrata a gente', 'odin' ), // Obrigatório
				    'type'        => 'text', // Obrigatório
				),
				array(
				    'id'          => 'email_contato', // Obrigatório
				    'label'       => __( 'Email', 'odin' ), // Obrigatório
				    'type'        => 'text', // Obrigatório
				),
				array(
				    'id'          => 'telefone_contato', // Obrigatório
				    'label'       => __( 'Telefone', 'odin' ), // Obrigatório
				    'type'        => 'text', // Obrigatório
				),
				array(
				    'id'          => 'endereco_contato', // Obrigatório
				    'label'       => __( 'Endereço', 'odin' ), // Obrigatório
				    'type'        => 'editor', // Obrigatório
				    'options'     => array( // Opcional (aceita argumentos do wp_editor)
				        'textarea_rows' => 10
				    ),
				),
				array(
				    'id'          => 'copyright_contato', // Obrigatório
				    'label'       => __( 'Copyright', 'odin' ), // Obrigatório
				    'type'        => 'text', // Obrigatório
				),
		    )
		);
	}






?>