<?php

	$portfolio = new Odin_Post_Type(
	    'Trabalho', // Nome (Singular) do Post Type.
	    'portfolio' // Slug do Post Type.
	);

	$portfolio->set_labels(
	    array(
	        'menu_name' => __( 'Trabalhos', 'odin' )
	    )
	);

	$portfolio->set_arguments(
	    array(
	        'supports' => array( 'title', 'editor', 'thumbnail' ),
	        'menu_icon' => 'dashicons-book-alt'
	    )
	);





?>