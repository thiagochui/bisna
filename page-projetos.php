<?php get_header(); ?>

	<div id="menu-projeto">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/topo-projeto.png">
		<nav class="navbar navbar-default">
		  	<div class="container">
			    <div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
			      	<a class="navbar-brand" href="/bisna"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-branca.png"></a>
			    </div>
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right animated fadeIn">
						<li><a href="/bisna">Início</a></li>
						<li><a data-scroll href="#portfolio">Portfólio</a></li>
						<li><a data-scroll href="#sobre">Sobre</a></li>
						<li><a data-scroll href="#contato">Contato</a></li>
					</ul>
			    </div><!-- /.navbar-collapse -->
			    <a class="menu-bar menu-skin-light hidden-xs" href="#menu-open">
					<span class="ham"></span>
				</a>
		  	</div><!-- /.container-->
		</nav>
	</div>

	<div class="conteudo-projeto">
		<div class="container">
			<p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá, depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
			<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/img-projeto-01.png"></a>
			<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/img-projeto-02.png"></a>
			<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/img-projeto-03.png"></a>
		</div>
	</div>

	
	<div class="container">
		<div class="rodape-projeto">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/b.png">
		</div>
	</div>

<?php get_footer(); ?>