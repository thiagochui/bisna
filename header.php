<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till #main div
 *
 * @package Odin
 * @since 2.2.0
 */
?><!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<title>Bisna</title>
	<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/main.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/libs/bootstrap.min.css"> 
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/libs/animate.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/libs/font-awesome.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/libs/owl.carousel.css">
	<link rel='shortcut icon' type='image/x-icon' href='<?php echo get_template_directory_uri(); ?>/assets/img/favicon.png' />
	<link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
	
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/libs/jquery-1.11.3.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/libs/bootstrap.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/libs/owl.carousel.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/libs/smooth-scroll.js"></script>	
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/libs/wow.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/libs/jquery.hoverdir.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/libs/modernizr.custom.js"></script>
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	
