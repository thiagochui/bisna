<?php
/**
 * Template Name: Home
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Odin
 * @since 2.2.0
 */

get_header(); ?>

<?php $pageID = get_the_ID(); ?>

	<div id="menu">
		<nav class="navbar navbar-default">
		  	<div class="container">
			    <div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
			      	<a class="navbar-brand" href="/bisna"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png"></a>
			    </div>
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right animated fadeIn">
						<li><a href="/bisna">Início</a></li>
						<li><a data-scroll href="#portfolio">Portfólio</a></li>
						<li><a data-scroll href="#sobre">Sobre</a></li>
						<li><a data-scroll href="#contato">Contato</a></li>
					</ul>
			    </div><!-- /.navbar-collapse -->
			    <a class="menu-bar menu-skin-light hidden-xs" href="#menu-open">
					<span class="ham"></span>
				</a>
		  	</div><!-- /.container-->
		</nav>
	</div>

	<div id="portfolio">
		<div class="container-fluid">
			<?php 
			$args = array(
				'posts_per_page'   => -1,
				'post_type'        => 'portfolio',
				'post_status'      => 'publish',
				'offset'           => 0
			);
			$posts_trabalhos = get_posts( $args );
			$image_equipe = array();
			foreach ($posts_trabalhos as $trabalho):
			?>
			<div class="col-xs-12 col-sm-4 col-md-3 colunas-portfolio">
				<div class="box-projetos">
	                <a href="<?php echo get_permalink( $trabalho ) ?>">
	                <?php 
						$equipe_meta = get_post_meta($trabalho->ID);
						array_push($image_equipe, $equipe_meta);
						if($equipe_meta['img_destaque']) { 
					?>
	                <img src="<?php echo wp_get_attachment_url($equipe_meta['img_destaque'][0]); ?>">
	                <?php } ?>
	                    <div class="mask">
	                        <h4><?php echo $trabalho->post_title; ?></h4>
	                    </div>
	                </a>
	            </div>
            </div>
        <?php endforeach; ?>  
		</div>
	</div>

	<div id="sobre">
		<div class="container">
			<h2>Sobre Nós</h2>
			<p><?php echo get_post_meta($pageID, 'sobre_nos', true) ?></p>
		</div>
	</div>

	<div id="padawans">
		<div class="container">
			<h2>Os Padawans</h2>
			<div class="col-xs-12 col-sm-6 col-md-6">
				<?php 
					$image_pad_1 = array();
					$img_padawan_1 = get_post_meta($pageID);
					array_push($image_pad_1, $img_padawan_1);
					if($img_padawan_1['img_padawan_1']) { 
				?>
				<img src="<?php echo wp_get_attachment_url($img_padawan_1['img_padawan_1'][0]); ?>">
				<?php } ?>
				<h4><?php echo get_post_meta($pageID, 'nome_padawan_1', true) ?></h4>
				<p><?php echo get_post_meta($pageID, 'texto_padawan_1', true) ?></p>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6">
				<?php 
					$image_pad_2 = array();
					$img_padawan_2 = get_post_meta($pageID);
					array_push($image_pad_2, $img_padawan_2);
					if($img_padawan_2['img_padawan_2']) { 
				?>
				<img src="<?php echo wp_get_attachment_url($img_padawan_2['img_padawan_2'][0]); ?>">
				<?php } ?>
				<h4><?php echo get_post_meta($pageID, 'nome_padawan_2', true) ?></h4>
				<p><?php echo get_post_meta($pageID, 'texto_padawan_2', true) ?></p>
			</div>
		</div>
	</div>

	<div id="contato">
		<div class="container">
			<h2><?php echo get_post_meta($pageID, 'texto_trabalhe', true) ?></h2>
			<h5><?php echo get_post_meta($pageID, 'texto_contrata', true) ?></h5>
			<div class="col-xs-12 col-sm-6 col-md-6">
				<p><img src="<?php echo get_template_directory_uri(); ?>/assets/img/mail.png"><?php echo get_post_meta($pageID, 'email_contato', true) ?></p>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6">
				<p><img src="<?php echo get_template_directory_uri(); ?>/assets/img/phone.png"><?php echo get_post_meta($pageID, 'telefone_contato', true) ?></p>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
