<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main div element.
 *
 * @package Odin
 * @since 2.2.0
 */
?>
<?php $pageID = get_the_ID(); ?>
	<div class="copyright">
		<div class="container">
			<div class="col-xs-12 col-md-6 endereco">
				<p><?php echo get_post_meta($pageID, 'endereco_contato', true) ?></p>
			</div>
			<div class="col-xs-12 col-md-6 nois">
				<p><?php echo get_post_meta($pageID, 'copyright_contato', true) ?></p>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/main.js"></script>

	<?php wp_footer(); ?>
</body>
</html>
